#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
ui <- navbarPage(
  title = "BRIC-built Machine Learning",
  tabPanel("Data", "data retrival"),
  tabPanel("Feature Calculation", "feature calculation"),
  tabPanel("Model-01", "model-01"),
  tabPanel("Model-02", "model-02")
)

# Define server logic required to draw a histogram
server <- function(input, output) {
   
}

# Run the application 
shinyApp(ui = ui, server = server)

